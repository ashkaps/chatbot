FROM python:3.6
COPY . /app
WORKDIR /app
RUN pip3.6 install -r requirements.txt
# EXPOSE 5000
ENTRYPOINT ["python3.6"]
CMD ["socket_interact_api.py"]