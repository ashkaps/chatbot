from flask import Flask, render_template, request, json, jsonify, make_response, session
import os
from datetime import timedelta
import csv
import random 
import os

import time
from flask_cors import CORS, cross_origin

import logging
import random
from argparse import ArgumentParser
from itertools import chain
from pprint import pformat
import warnings

import torch
import torch.nn.functional as F

from transformers import OpenAIGPTLMHeadModel, OpenAIGPTTokenizer, GPT2LMHeadModel, GPT2Tokenizer
from train import SPECIAL_TOKENS, build_input_from_segments, add_special_tokens_
from utilsnew import get_dataset, download_pretrained_model
import csv
import nltk
from nltk.tag import StanfordNERTagger
import traceback 




names = list()
def read_names():
    with open('names.csv') as csvfile:
        csvfile = csv.reader(csvfile)
        for r in csvfile:
            names.append(r[0])

    print(str(len(names)) + " names")


logging.basicConfig(filename="newfile.log", 
                    format='%(asctime)s %(message)s', 
                    filemode='w') 
  #Creating an object 
logger=logging.getLogger() 
  
#Setting the threshold of logger to DEBUG 
logger.setLevel(logging.DEBUG) 

app = Flask(__name__)
app.config['SECRET_KEY'] = os.urandom(24)
app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(days = 7)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'





def top_filtering(logits, top_k=0., top_p=0.9, threshold=-float('Inf'), filter_value=-float('Inf')):
    """ Filter a distribution of logits using top-k, top-p (nucleus) and/or threshold filtering
        Args:
            logits: logits distribution shape (vocabulary size)
            top_k: <=0: no filtering, >0: keep only top k tokens with highest probability.
            top_p: <=0.0: no filtering, >0.0: keep only a subset S of candidates, where S is the smallest subset
                whose total probability mass is greater than or equal to the threshold top_p.
                In practice, we select the highest probability tokens whose cumulative probability mass exceeds
                the threshold top_p.
            threshold: a minimal threshold to keep logits
    """
    assert logits.dim() == 1  # Only work for batch size 1 for now - could update but it would obfuscate a bit the code
    top_k = min(top_k, logits.size(-1))
    if top_k > 0:
        # Remove all tokens with a probability less than the last token in the top-k tokens
        indices_to_remove = logits < torch.topk(logits, top_k)[0][..., -1, None]
        logits[indices_to_remove] = filter_value

    if top_p > 0.0:
        # Compute cumulative probabilities of sorted tokens
        sorted_logits, sorted_indices = torch.sort(logits, descending=True)
        cumulative_probabilities = torch.cumsum(F.softmax(sorted_logits, dim=-1), dim=-1)

        # Remove tokens with cumulative probability above the threshold
        sorted_indices_to_remove = cumulative_probabilities > top_p
        # Shift the indices to the right to keep also the first token above the threshold
        sorted_indices_to_remove[..., 1:] = sorted_indices_to_remove[..., :-1].clone()
        sorted_indices_to_remove[..., 0] = 0

        # Back to unsorted indices and set them to -infinity
        indices_to_remove = sorted_indices[sorted_indices_to_remove]
        logits[indices_to_remove] = filter_value

    indices_to_remove = logits < threshold
    logits[indices_to_remove] = filter_value

    return logits


def sample_sequence(personality, history, tokenizer, model, args, current_output=None):
    special_tokens_ids = tokenizer.convert_tokens_to_ids(SPECIAL_TOKENS)
    if current_output is None:
        current_output = []

    for i in range(args.max_length):
        instance = build_input_from_segments(personality, history, current_output, tokenizer, with_eos=False)

        input_ids = torch.tensor(instance["input_ids"], device=args.device).unsqueeze(0)
        token_type_ids = torch.tensor(instance["token_type_ids"], device=args.device).unsqueeze(0)

        logits = model(input_ids, token_type_ids=token_type_ids)
        if isinstance(logits, tuple):  # for gpt2 and maybe others
            logits = logits[0]
        logits = logits[0, -1, :] / args.temperature
        logits = top_filtering(logits, top_k=args.top_k, top_p=args.top_p)
        probs = F.softmax(logits, dim=-1)

        prev = torch.topk(probs, 1)[1] if args.no_sample else torch.multinomial(probs, 1)
        if i < args.min_length and prev.item() in special_tokens_ids:
            while prev.item() in special_tokens_ids:
                if probs.max().item() == 1:
                    warnings.warn("Warning: model generating special token with probability 1.")
                    break  # avoid infinitely looping over special token
                prev = torch.multinomial(probs, num_samples=1)

        if prev.item() in special_tokens_ids:
            break
        current_output.append(prev.item())

    return current_output


parser = ArgumentParser()
parser.add_argument("--dataset_path", type=str, default="", help="Path or url of the dataset. If empty download from S3.")
parser.add_argument("--dataset_cache", type=str, default='./dataset_cache', help="Path or url of the dataset cache")
parser.add_argument("--model", type=str, default="openai-gpt", help="Model type (openai-gpt or gpt2)", choices=['openai-gpt', 'gpt2'])  # anything besides gpt2 will load openai-gpt
parser.add_argument("--model_checkpoint", type=str, default="", help="Path, url or short name of the model")
parser.add_argument("--max_history", type=int, default=30, help="Number of previous utterances to keep in history")
parser.add_argument("--device", type=str, default="cuda" if torch.cuda.is_available() else "cpu", help="Device (cuda or cpu)")

parser.add_argument("--no_sample", action='store_true', help="Set to use greedy decoding instead of sampling")
parser.add_argument("--max_length", type=int, default=40, help="Maximum length of the output utterances")
parser.add_argument("--min_length", type=int, default=3, help="Minimum length of the output utterances")
parser.add_argument("--seed", type=int, default=0, help="Seed")
parser.add_argument("--temperature", type=float, default=0.7, help="Sampling softmax temperature")
parser.add_argument("--top_k", type=int, default=0, help="Filter top-k tokens before sampling (<=0: no filtering)")
parser.add_argument("--top_p", type=float, default=0.9, help="Nucleus filtering (top-p) before sampling (<=0.0: no filtering)")
args = parser.parse_args()

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__file__)
logger.info(pformat(args))

if args.model_checkpoint == "":
    if args.model == 'gpt2':
        raise ValueError("Interacting with GPT2 requires passing a finetuned model_checkpoint")
    else:
        args.model_checkpoint = download_pretrained_model()


if args.seed != 0:
    random.seed(args.seed)
    torch.random.manual_seed(args.seed)
    torch.cuda.manual_seed(args.seed)


logger.info("Get pretrained model and tokenizer")
tokenizer_class, model_class = (GPT2Tokenizer, GPT2LMHeadModel) if args.model == 'gpt2' else (OpenAIGPTTokenizer, OpenAIGPTLMHeadModel)
tokenizer = tokenizer_class.from_pretrained(args.model_checkpoint)
model = model_class.from_pretrained(args.model_checkpoint)
model.to(args.device)
add_special_tokens_(model, tokenizer)
dataset = get_dataset(tokenizer, args.dataset_path, args.dataset_cache)
personalities = [dialog["personality"] for dataset in dataset.values() for dialog in dataset]

def run(prompt, personality, history): 
    
    raw_text = prompt
    history.append(tokenizer.encode(raw_text))
    with torch.no_grad():
        out_ids = sample_sequence(personality, history, tokenizer, model, args)
    history.append(out_ids)
    history = history[-(2*args.max_history+1):]
    out_text = tokenizer.decode(out_ids, skip_special_tokens=True)
    print(' BOT >>>>>'+out_text)  
    return out_text, history


@app.route('/health', methods=['GET'])
def test():
    return 'ok'

@app.route('/bot/name', methods=['POST'])
def get_name():
    try:
        global names
        req_data = request.get_json()
        if req_data is None:
            return jsonify(error='No request data. Send sid and message.',status=400,mimetype='application/json')
        sid = req_data['sid']
        if sid is None:
            return jsonify(error='sid value not sent in request',status=400,mimetype='application/json')
        response = jsonify(sid=sid, bot_name = random.choice(names), status=200,mimetype='application/json')
        return response
    except Exception as e:
        print(e)
st = StanfordNERTagger('stanford-ner/english.all.3class.distsim.crf.ser.gz', 'stanford-ner/stanford-ner.jar')
def get_human_names(text):
    person_list = list()
    for sent in nltk.sent_tokenize(text):
        tokens = nltk.tokenize.word_tokenize(sent)
        tags = st.tag(tokens)
        
        for tag in tags:
            if tag[1]=='PERSON': 
                person_list.append(tag[0])
    return person_list

def replace_fake_name(message, bot_name):
    message = message.upper()
    try:
        names = get_human_names(message)
        if len(names) > 0:
            replaced = message.replace(names[0],bot_name)
            return replaced.capitalize()
        else:
            return message.capitalize()
    except Exception as e:
        print(e)
        traceback.print_exc() 
        return message.capitalize()

# route and function to handle the upload page
@app.route('/bot/interact', methods=['POST'])
def bot_interact():
    try:
        
        req_data = request.get_json()
        if req_data is None:
            return jsonify(error='No request data. Send sid and message.',status=400,mimetype='application/json')
        
        sid = req_data['sid']
        user_message = req_data['user_message']
        bot_name = req_data['bot_name']
        if sid is None:
            return jsonify(error='sid value not sent in request',status=400,mimetype='application/json')
        if user_message is None:
            return jsonify(error='user_message value not sent in request',status=400,mimetype='application/json')
        
        global a
        history = []
        personality = None
        msg_exchanged = 0
        if sid in session:
            print(session.keys())
            history = a[sid]['history']
            personality = a[sid]['personality']
            msg_exchanged = a[sid]['msg_exchanged']
        else:
            session[sid] = sid
            logger.info("Choosing a personality for " + str(sid))    
            personality = random.choice(personalities)
            logger.info("Selected personality: %s", tokenizer.decode(chain(*personality)))
            # generate this user's variable
            print(session.keys())
            a[sid] = dict()
            a[sid]['personality'] = personality
            a[sid]['history'] = history
            a[sid]['msg_exchanged'] = 0
        prompt = str(user_message)
        print(prompt)
        bot_message, history = run(prompt, personality, history)
        a[sid]['history'] = history
        print("Socket ID: " , sid)
        print(str(sid) + ' >> ' + bot_message + ' >> ' + str(history))
        # socket_.to(sid).emit('reply', 'hello sam')
        msg_exchanged = msg_exchanged + 1
        a[sid]['msg_exchanged'] = msg_exchanged
        disconnect = False
        if msg_exchanged > 5 and msg_exchanged <30 and random.choice([True, False,True, False,True, False,True, False,True, False,True, False]):
            disconnect = True
        words_cnt = len(bot_message.split())
        # time.sleep(words_cnt/2)
        response = jsonify(bot_message=replace_fake_name(bot_message, bot_name), sid=sid, user_message = user_message, disconnect=disconnect, status=200,mimetype='application/json')
        return response
    except Exception as e:
        print(e)



if __name__ == '__main__':
    a = {}
    read_names()
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port, debug=True)